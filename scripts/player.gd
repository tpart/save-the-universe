extends KinematicBody


const GRAVITY = -20
const MAX_SPEED = 8.5
const ACCEL = 4
const AIR_ACCEL = 1
const DEACCEL= 16
const AIR_DEACCEL = 2.5
const JUMP_POWER = 8
const MAX_SLOPE_ANGLE = 90
const MAX_TERRAIN_DELTA = 0.4
const MOUSE_SENSITIVITY = 0.05
const CONTROLLER_MOUSE_SENSITIVITY_MULTIPLIER = 40

var vel = Vector3.ZERO
var mouse_sensitivity_multiplier = 1
var game_paused := false
var footstep_sounds = [preload("res://assets/sound/footsteps/1.ogg"), preload("res://assets/sound/footsteps/2.ogg"), preload("res://assets/sound/footsteps/3.ogg"), preload("res://assets/sound/footsteps/4.ogg")]

export(NodePath) var interiort

onready var footstep_sound = $footstep
onready var footstep_timer = $footstep_timer
onready var in_game_ui = $InGameUi
onready var rotation_helper = $RotationHelper
onready var camera = $RotationHelper/Camera
onready var ray = $RotationHelper/Camera/RayCast
onready var distortion = $Distortion
onready var distortion_animation = $Distortion/AnimationPlayer
onready var interior: Interior = get_node(interiort)


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	Global.try(Global.connect("weirdness_changed", self, "_on_weirdness_changed"))
	if Global.weirdness >= Global.WeirdLevel.Aglkasmnil:
		distortion.show()
		distortion_animation.play("introduce_distortion")


func _physics_process(delta):
	# Calculate movement vector
	var dir = Vector3.ZERO
	var input_movement_vector = Vector2.ZERO
	if Input.is_action_pressed("move_forward") or Input.is_action_pressed("move_backwards"):
		input_movement_vector.y = Input.get_action_strength("move_forward") - Input.get_action_strength("move_backwards")
	if Input.is_action_pressed("move_right") or Input.is_action_pressed("move_left"):
		input_movement_vector.x =  Input.get_action_strength("move_left") - Input.get_action_strength("move_right")
	input_movement_vector = input_movement_vector.normalized()
	if Input.is_action_pressed("move_forward") or Input.is_action_pressed("move_backwards"):
		dir.z = input_movement_vector.y * Input.get_action_strength("move_forward") - Input.get_action_strength("move_backwards")
	if Input.is_action_pressed("move_right") or Input.is_action_pressed("move_left"):
		dir.x = input_movement_vector.x * Input.get_action_strength("move_left") - Input.get_action_strength("move_right")
	dir = dir.rotated(Vector3.UP, rotation.y)
	
	# Jumping
	if is_on_floor():
		if Input.is_action_just_pressed("jump"):
			if Global.weirdness > Global.WeirdLevel.Normal:
				vel.y = JUMP_POWER
	
	# Looking around with controller
	if (Input.is_action_pressed("look_up") or Input.is_action_pressed("look_down") or Input.is_action_pressed("look_left") or Input.is_action_pressed("look_right")):
		rotation_helper.rotate_x(deg2rad((Input.get_action_strength("look_down") - Input.get_action_strength("look_up")) * MOUSE_SENSITIVITY * mouse_sensitivity_multiplier * CONTROLLER_MOUSE_SENSITIVITY_MULTIPLIER))
		self.rotate_y(deg2rad((Input.get_action_strength("look_left") - Input.get_action_strength("look_right")) * MOUSE_SENSITIVITY * mouse_sensitivity_multiplier * CONTROLLER_MOUSE_SENSITIVITY_MULTIPLIER))
		# Clamp rotation
		var camera_rot = rotation_helper.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -80, 80)
		rotation_helper.rotation_degrees = camera_rot
	
	# Processing movement
	dir.y = 0
	if !is_on_floor():
		vel.y += delta * GRAVITY
	if vel.y < -50:
		Global.try(get_tree().reload_current_scene())
	var hvel = vel
	hvel.y = 0
	var target = dir * MAX_SPEED
	var accel
	if dir.dot(hvel) > 0:
		if is_on_floor():
			accel = ACCEL
		else:
			accel = AIR_ACCEL
	else:
		if is_on_floor():
			accel = DEACCEL
		else:
			accel = AIR_DEACCEL
	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel, Vector3(0, 1, 0), true, 4, deg2rad(MAX_SLOPE_ANGLE), true)
	
	ray.force_raycast_update()
	
	var coll = ray.get_collider()
	if coll != null:
		if coll.get_parent().is_in_group("door"):
			var door: MeshInstance = coll.get_parent()
			var door_tile: TileNode = door.get_parent()
			if Input.is_action_just_pressed("activate"):
				interior.on_door_open(door_tile)
				$Message.hide()
		elif coll is CodeButton:
			if Input.is_action_just_pressed("activate"):
				coll.press()


	# Escape menu
	if Input.is_action_just_pressed("ui_cancel"):
		if not game_paused:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			in_game_ui.show()
			game_paused = true
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			in_game_ui.hide()
			game_paused = false


func _input(event):
	if event is InputEventMouseMotion:
		rotation_helper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY * mouse_sensitivity_multiplier))
		rotation_helper.rotation_degrees.x = clamp(rotation_helper.rotation_degrees.x, -80, 80)
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * mouse_sensitivity_multiplier * -1))


func continueg():
	in_game_ui.hide()
	game_paused = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func menu():
	Global.try(get_tree().change_scene("res://scenes/main_menu.tscn"))


func footstep():
	footstep_sound.stream = footstep_sounds[randi() % footstep_sounds.size()]
	footstep_sound.play()


func _on_footstep_timer_timeout():
	if Input.is_action_pressed("move_forward") or Input.is_action_pressed("move_left") or Input.is_action_pressed("move_right") or Input.is_action_pressed("move_backwards"):
		footstep()


func _on_weirdness_changed(weirdness):
	if weirdness >= Global.WeirdLevel.Aglkasmnil:
		distortion.show()
		distortion_animation.play("introduce_distortion")
