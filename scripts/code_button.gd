class_name CodeButton
extends StaticBody


signal value_pressed

export var value: int


func _ready():
	pass


func press():
	get_parent().get_parent().get_node("Anim").play(str(value))
	emit_signal("value_pressed", value)
	Global.push_digit(value)
	$beep.play()
