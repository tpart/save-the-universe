extends Spatial


const ROTATION_SPEED = 20

onready var blades = $HelicopterBlades


func _process(delta):
	blades.rotation.y += delta * ROTATION_SPEED
