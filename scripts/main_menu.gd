extends Control


const CAMERA_SHAKE_SPEED = 20
const CAMERA_SHAKE_INTENSITY = 0.02

var noise = OpenSimplexNoise.new()

onready var menu_ambience = $menu_ambience
onready var menu_credits = $menu_credits
onready var camera_3d = $"3d/Camera"
onready var camera_start_pos = camera_3d.global_transform.origin


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


var t = 0
func _process(delta):
	t += delta
	camera_3d.global_transform.origin.x = camera_start_pos.x + noise.get_noise_3d(t * CAMERA_SHAKE_SPEED, 0, 0) * CAMERA_SHAKE_INTENSITY * 10
	camera_3d.global_transform.origin.y = camera_start_pos.y + noise.get_noise_3d(0, t * CAMERA_SHAKE_SPEED, 0) * CAMERA_SHAKE_INTENSITY * 10
	camera_3d.global_transform.origin.z = camera_start_pos.z + noise.get_noise_3d(0, 0, t * CAMERA_SHAKE_SPEED) * CAMERA_SHAKE_INTENSITY * 10


func _on_Quit_pressed():
	get_tree().quit()


func _on_Play_pressed():
	Global.generate_code()
	Global.try(get_tree().change_scene("res://scenes/intro.tscn"))


func _on_Credits_pressed():
	menu_credits.generate()
	menu_credits.show()
