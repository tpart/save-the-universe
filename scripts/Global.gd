extends Node

signal wrong
signal right
signal weirdness_changed

var code = []
var buf = []
var code_numbers = [1, 2, 3, 4]
var weirdness: int = WeirdLevel.Normal

var web := false # to be used for detecting disabling ssao (?) on chromium

enum WeirdLevel {
	Normal
	Unexpected
	Shit
	Goddammit
	Aglkasmnil
}
var iweird := 0


func _ready():
	generate_code()
	web = OS.get_name() == "HTML5"


func generate_code():
	randomize()
	code = []
	for _i in range(4):
		code.push_back(code_numbers[randi() % code_numbers.size()])
	buf = []
	weirdness = WeirdLevel.Normal
	iweird = 0


func _input(_event):
	if Input.is_action_just_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen


func weirder(fac: int):
	iweird += fac
	var current_weirdness = weirdness
	if iweird > 150:
		weirdness = WeirdLevel.Aglkasmnil
	elif iweird > 100:
		weirdness = WeirdLevel.Goddammit
	elif iweird > 75:
		weirdness = WeirdLevel.Shit
	elif iweird > 30:
		weirdness = WeirdLevel.Unexpected
	if current_weirdness != weirdness:
		emit_signal("weirdness_changed", weirdness)

func push_digit(d: int):
	weirder(5)
	if buf.size() > 2:
		buf.push_back(d)
		if buf == code:
			emit_signal("right")
			try(get_tree().change_scene("res://scenes/success.tscn"))
			print("YEAH")
		else:
			buf.clear()
			emit_signal("wrong")
			print("wrong")
			weirder(35)
			
	else:
		buf.push_back(d)


func try(e: int):
	if e != OK:
		assert(e == OK) # for debugging in editor
		OS.alert("something went horribly wrong, error: %s" % e)
		get_tree().quit() # OS.crash not working, idk why
