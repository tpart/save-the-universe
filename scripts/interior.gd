class_name Interior

extends Spatial


enum TileTypes {
	Stairs
	Intersection
	CorridorL
	CorridorT
	CorridorI
}

const tiles = {
	TileTypes.Stairs: preload("res://assets/tiles/staircase/stairase.tscn"),
	TileTypes.Intersection: preload("res://assets/tiles/corridor-intersection/corridor-intersection.tscn"),
	TileTypes.CorridorL: preload("res://assets/tiles/corridor-corner/corridor-corner.tscn"),
	TileTypes.CorridorT: preload("res://assets/tiles/corridor-t-intersection/corridor-t-intersection.tscn"),
	TileTypes.CorridorI: preload("res://assets/tiles/corridor/corridor.tscn"),
}

enum RoomTypes {
	Corridor,
	Staircase,
	Office,
	Conference,
	Spooky,
}


const rooms = {
	RoomTypes.Office: {"P": 0.6, "func": "generate_office"},
	RoomTypes.Conference: {"P": 0.4, "func": "generate_conference"},
	RoomTypes.Corridor: {"P": 0.3, "func": "generate_corridor"},
	RoomTypes.Staircase: {"P": 0.1, "func": "generate_staircase"},
	RoomTypes.Spooky: {"P": 0.1, "func": "generate_spooky"},
}


var current_room: GameRoom
var next_room: GameRoom
var can_open := true


func _ready():
	randomize()
	current_room = GameRoom.new(0)
	add_new_room(RoomTypes.Office, -2, 0, 0, 0)
	add_child(current_room)
	on_door_closed()


func on_door_open(door: Door):
	if can_open:
		var roll_of_the_dice: int = randi() % 6 + 1
		var next_type: int
		if current_room.room_type != RoomTypes.Corridor:
			next_type = RoomTypes.Corridor
		elif roll_of_the_dice > 4:
			next_type = RoomTypes.Office
		elif roll_of_the_dice > 3:
			next_type = RoomTypes.Corridor
		else:
			next_type = RoomTypes.Staircase
		
		add_new_room(next_type, door.x, door.y, door.z, door.rot)
		door.open_door()
		
		Global.try(door.connect("door_closed", self, "on_door_closed"))
		can_open = false


func add_new_room(roomtype: int,
	doorx: int, doory: int, doorz: int, doorrot: int):
	next_room = GameRoom.new(roomtype)

	call(rooms[roomtype]["func"], next_room, doorx, doory, doorz, doorrot)
	add_child(next_room)


func on_door_closed():
	current_room.queue_free()
	current_room = next_room
	next_room = null
	
	current_room.add_missing_door()
	can_open = true
	Global.weirder(1)


func generate_corridor(room: GameRoom, doorx: int, doory: int, doorz: int, doorrot: int):
	var roll_of_the_dice2: int = (randi() % 12 + 1) * int((Global.weirdness * 0.5) + 1)
	doorrot = posmod(doorrot, 4)
	var tl = roll_of_the_dice2
	var tw = 1
	
	for l in range(0, tl + 1):
		for w in range(0, tw):
			var current_tile: Adoorable
			var x
			var z
			match doorrot:
				0:
					x = l + doorx + 1
					z = w + doorz
				1:
					x = w + doorx
					z = -l + doorz - 1
				2:
					x = -l + doorx - 1
					z = -w + doorz
				3:
					x = -w + doorx
					z = l + doorz + 1

			if l == 0:
				var node = tiles[TileTypes.Intersection].instance()
				node.init(x, doory, z, doorrot)
				current_tile = node
				room.push(node)
				room.set_door(node, doorrot)
			if l % 2 == 0:
				var roll_of_the_dice: int = randi() % 6 + 1
				
				if roll_of_the_dice == 1:
					var node = tiles[TileTypes.CorridorT].instance()
					node.init(x, doory, z, doorrot - 1)
					node.add_new_door(doorrot - 1, room)
					current_tile = node
					room.push(node)
				elif roll_of_the_dice == 3:
					var node = tiles[TileTypes.CorridorT].instance()
					node.init(x, doory, z, doorrot + 1)
					node.add_new_door(doorrot + 1, room)
					current_tile = node
					room.push(node)
				elif roll_of_the_dice == 6:
					var node = tiles[TileTypes.Intersection].instance()
					node.init(x, doory, z, doorrot)
					node.add_new_door(doorrot - 1, room)
					node.add_new_door(doorrot + 1, room)
					current_tile = node
					room.push(node)
				else:
					var node = tiles[TileTypes.CorridorI].instance()
					node.init(x, doory, z, doorrot)
					current_tile = node
					room.push(node)
				current_tile.show_lamp()
			else:
				var node: Adoorable = tiles[TileTypes.CorridorI].instance()
				var roll_of_the_dice3: int = randi() % 12 + 1
				node.init(x, doory, z, doorrot)
				if roll_of_the_dice3 == 3:
					node.add_new_poster(doorrot, room)
				elif roll_of_the_dice3 == 4:
					node.add_new_poster(doorrot + 2, room)
				current_tile = node
				room.push(node)
			if l == tl:
				current_tile.add_new_door(doorrot, room)


func generate_office(room: GameRoom, doorx: int, doory: int, doorz: int, doorrot: int):
	doorrot = posmod(doorrot, 4)
	
	var tw = 3
	var tl = 4

	var machine := false
	for w in range(0, tw):
		for l in range(0, tl):
			var x
			var z
			match doorrot:
				0:
					x = l + doorx + 1
					z = w + doorz - 1
				1:
					x = w + doorx - 1
					z = -l + doorz - 1
				2:
					x = -l + doorx - 1
					z = -w + doorz + 1
				3:
					x = -w + doorx + 1
					z = l + doorz + 1
			
			if w == 0:
				wallx(room, l, tl, x, z, doorrot, doory)
			elif abs(w) < tw - 1:
				if w == 1 && l == 0:
					var node = tiles[TileTypes.Intersection].instance()
					room.set_door(node, doorrot)
					node.init(x, doory, z, 0)
					room.push(node)
				else:
					if not machine:
						machine = middlex(room, l, tl, x, z, doorrot, doory)
					else:
						middlex(room, l, tl, x, z, doorrot, doory)
			else:
				wallxe(room, l, tl, x, z, doorrot, doory, machine)


func wallx(room: GameRoom, l: int, tl: int, x: int, z: int, rot: int, h: int):
	var roll_of_the_dice: int = randi() % 6 + 1
	var node: Adoorable
	if l == 0:
		node = tiles[TileTypes.CorridorL].instance()
		node.init(x, h, z, rot + 0)
		room.push(node)
	elif l < tl - 1:
		node = tiles[TileTypes.CorridorT].instance()
		node.init(x, h, z, rot + 3)
		room.push(node)
	else:
		node = tiles[TileTypes.CorridorL].instance()
		node.init(x, h, z, rot + 3)
		room.push(node)
	
	if roll_of_the_dice > 3:
		node.add_new_bookshelf(rot, room)


func middlex(room: GameRoom, l: int, tl: int, x: int, z: int, rot: int, h: int):
	var roll_of_the_dice: int = randi() % 5 + 1
	var machine := false
	if l == 0:
		# DOOR
		var node = tiles[TileTypes.CorridorT].instance()
		node.init(x, h, z, rot + 0)
		room.push(node)
	elif l < tl - 1:
		var node: Adoorable = tiles[TileTypes.Intersection].instance()
		node.init(x, h, z, rot + 0)
		if l % 2 == 0:
			if roll_of_the_dice == 2:
				node.add_new_evil(rot, room)
				machine = true
			else:
				node.show_lamp()
		room.push(node)
	else:
		var node: Adoorable = tiles[TileTypes.Intersection].instance()
		node.init(x, h, z, rot + 2)
		node.add_new_window(rot + 2, room)
		room.push(node)
	
	return machine

func wallxe(room: GameRoom, l: int, tl: int, x: int, z: int, rot: int, h: int, machine: bool):
	var roll_of_the_dice: int = randi() % 6 + 1
	var node: Adoorable
	if l == 0:
		node = tiles[TileTypes.CorridorL].instance()
		node.init(x, h, z, rot + 1)
		room.push(node)
	elif l < tl - 1:
		node = tiles[TileTypes.CorridorT].instance()
		node.init(x, h, z, rot + 1)
		room.push(node)
		
		if roll_of_the_dice > 2 and !machine:
			node.add_new_desk(rot + 2, room)
		
	else:
		node = tiles[TileTypes.CorridorL].instance()
		node.init(x, h, z, rot + 2)
		room.push(node)


func generate_staircase(room: GameRoom, doorx: int, doory: int, doorz: int, doorrot: int):
	var roll_of_the_dice: int = (randi() % 6 + 1) * int((Global.weirdness * 0.5) + 1)
	doorrot = posmod(doorrot, 4)
	var tl = roll_of_the_dice
	var tw = 1
	var y = doory
	var dir: int
	var roll_of_the_dice2: int = randi() % 6 + 1
	
	if roll_of_the_dice2 > 3:
		dir = -1
	else:
		dir = 1
	
	for l in range(0, tl + 1):
		for w in range(0, tw):
			var x
			var z
			match doorrot:
				0:
					x = l + doorx + 1
					z = w + doorz
				1:
					x = w + doorx
					z = -l + doorz - 1
				2:
					x = -l + doorx - 1
					z = -w + doorz
				3:
					x = -w + doorx
					z = l + doorz + 1
			
			if l == 0:
				var node: Adoorable = tiles[TileTypes.CorridorI].instance()
				node.init(x, y, z, doorrot)
				room.push(node)
				node.show_lamp()
				room.set_door(node, doorrot)
			elif l < tl:
				var node: Stairs = tiles[TileTypes.Stairs].instance()
				if dir == -1:
					y += dir
					node.init(x, y, z, doorrot)
				else:
					node.init(x, y, z, doorrot + 2)
					y += dir
				
				node.show_wall()
				room.push(node)
			else:
				var node: Adoorable = tiles[TileTypes.CorridorI].instance()
				node.init(x, y, z, doorrot)
				node.add_new_door(doorrot, room)
				node.show_lamp()
				room.push(node)
