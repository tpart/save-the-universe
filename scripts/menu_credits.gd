extends Panel


onready var credits_text = $VBoxContainer/ScrollContainer/credits_text


func _on_Back_pressed():
	hide()


func generate():
	if not credits_text.generated:
		credits_text.generate_text()
