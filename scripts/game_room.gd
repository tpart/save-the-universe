class_name GameRoom

extends Spatial

const DRIFT := 0.2

var stack = []

var door_tile: Adoorable
var doorrot: int

var room_type: int


func _init(_type: int):
	room_type = _type

func set_door(_door_tile: Adoorable, _doorrot: int):
	door_tile = _door_tile
	doorrot = _doorrot

func push(t: TileNode):
	stack.push_back(t)


func _ready():
	for i in stack:
		add_child(i)


func _physics_process(delta):
	if Global.weirdness >= Global.WeirdLevel.Shit:
		for i in stack:
			i.rotation.y += 0.05 * delta
			
	if Global.weirdness >= Global.WeirdLevel.Goddammit:
		for i in stack:
			i.rotation.x += 0.02 * delta
	
	if Global.weirdness >= Global.WeirdLevel.Unexpected:
		for i in stack:
			i.translate(Vector3(
				rand_range(-DRIFT,DRIFT), rand_range(-DRIFT,DRIFT), rand_range(-DRIFT,DRIFT))
				* delta * float(Global.weirdness))

func add_missing_door():
	door_tile.add_new_door(doorrot + 2, self)
