class_name TileNode
extends Spatial

var x: int
var y: int
var z: int

var rot: int


const tile_scale = 5.0
const LAMP = preload("res://assets/tiles/lamp/lamp.tscn")


func init(newx: int, newy: int, newz: int, newrot: int):
	x = newx
	y = newy
	z = newz
	
	rot = newrot


func _ready():
	translation = Vector3(x * tile_scale, y * tile_scale, z * tile_scale)
	rotation_degrees.y = rot * 90


func show_lamp():
	add_child(LAMP.instance())
