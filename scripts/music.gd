extends Node


const WEIRD_MUSIC = {
	Global.WeirdLevel.Goddammit : preload("res://assets/sound/music/symmetry.ogg")
}

onready var music_player = $MusicPlayer


func _ready():
	Global.try(Global.connect("weirdness_changed", self, "_on_weirdness_changed"))


func _on_weirdness_changed(weirdness):
	if WEIRD_MUSIC.has(weirdness):
		if music_player.playing:
			music_player.stop()
		music_player.stream = WEIRD_MUSIC[weirdness]
		music_player.play()
