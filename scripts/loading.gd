extends Control


const LOADING_SPINNER_SPEED = 400
const BACKGROUND_LOADING = true

onready var loading_spinner = $loading_anchor/loading_spinner


func _ready():
	if OS.can_use_threads():
		BackgroundLoader.load_scene("res://scenes/main.tscn")
	else:
		Global.try(get_tree().change_scene("res://scenes/main.tscn"))


func _process(delta):
	loading_spinner.rotation_degrees += delta * LOADING_SPINNER_SPEED
