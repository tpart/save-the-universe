extends Spatial


func _ready():
	if Global.weirdness >= Global.WeirdLevel.Aglkasmnil:
		randomize()
		var roll_of_the_dice: int = randi() % 5
		if roll_of_the_dice == 1:
			$Dark.show()
			$Normal.hide()
		elif roll_of_the_dice != 4:
			$Red.show()
			$Normal.hide()
