extends TileNode


onready var dice = $dice

const DICE_SPEED = 25


func _process(delta):
	dice.rotation.y += DICE_SPEED * delta
