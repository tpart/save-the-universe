extends Label


const KEVIN_MAC_LEOD_TRACKS = ["Symmetry"]
const KEVIN_MAC_LEOD_LICENSE = '"%s" Kevin MacLeod (incompetech.com)\nLicensed under Creative Commons: By Attribution 4.0 License\nhttp://creativecommons.org/licenses/by/4.0/\n'

const CC_BY_4_CONTENT = [["Ambience, London Street, A", "InspectorJ"], ["HELICOPTER (sound)", "isaac_arva"], ["wind", "ERH"]]
const CC_BY_4 = '"%s" %s\nLicensed under Creative Commons: By Attribution 4.0 License\nhttp://creativecommons.org/licenses/by/4.0/\n'

const CC_BY_3_CONTENT = [["Helicopter", "jeremy"], ["Desk", "jeremy"], ["Dice", "Jakob Hippe"], ["Computer", "Poly by Google"], ["Simple Computer", " Robert Schlyter"], ["BEEP 3", "anthonychartier2020"], ["Error", "Autistic Lucario"]]
const CC_BY_3 = '"%s" %s\nLicensed under Creative Commons: By Attribution 3.0 License\nhttp://creativecommons.org/licenses/by/3.0/\n'

const AUTHORS = ["tpart", "nokoe"]

var generated := false


func generate_text():
	text = "The Top Secret Mission To Save The Universe\n\n"
	text += "Programming & Design: %s\n\n" % str(AUTHORS)
	
	for i in KEVIN_MAC_LEOD_TRACKS:
		text += KEVIN_MAC_LEOD_LICENSE % i + "\n"
	for i in CC_BY_4_CONTENT:
		text += CC_BY_4 % i + "\n"
	for i in CC_BY_3_CONTENT:
		text += CC_BY_3 % i + "\n"
	generated = true


func _on_Copy_pressed():
	OS.clipboard = text
