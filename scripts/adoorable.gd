class_name Adoorable

extends TileNode


const DOOR = preload("res://assets/tiles/door/door.tscn")
const WIND = preload("res://assets/tiles/window/window.tscn")
const SHELF = preload("res://assets/models/bookshelf/bookshelf.tscn")
const DESK = preload("res://assets/models/desk/desk.tscn")
const EVIL = preload("res://assets/models/evil machine/evil_machine.tscn")
const POSTER = preload("res://assets/models/instructions/instructions.tscn")

var doors := {}


func _ready():
	pass


# "root" node is needed for right translation
func add_new_door(doorrot: int, root: Spatial):
	if !doors.has(doorrot):
		var node = DOOR.instance()
		node.init(x, y, z, doorrot)
		root.add_child(node)
		doors[doorrot] = node
	else:
		push_error("door already exists")


func add_new_bookshelf(doorrot: int, root: Spatial):
	var node = SHELF.instance()
	node.init(x, y, z, doorrot)
	root.add_child(node)



func add_new_desk(doorrot: int, root: Spatial):
	var node = DESK.instance()
	node.init(x, y, z, doorrot)
	root.add_child(node)


func add_new_evil(doorrot: int, root: Spatial):
	var node = EVIL.instance()
	node.init(x, y, z, doorrot)
	root.add_child(node)


func add_new_window(doorrot: int, root: Spatial):
	var node = WIND.instance()
	node.init(x, y, z, doorrot)
	root.add_child(node)


func add_new_poster(doorrot: int, root: Spatial):
	var node = POSTER.instance()
	node.init(x, y, z, doorrot + 1)
	root.add_child(node)
