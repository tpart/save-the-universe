extends Control


const INTRO_TEXT = {
	"Hello, Agent 108." : 1,
	"We've received highly confidential information that Dr. Evil,\nthe crazy scientist who owns Evil Inc.," : 1.5,
	"is building a dangerous machine that will destroy the universe.": 2,
	"Your task is simple:": 1,
	"Break into the skyscraper, find the machine and destroy it\nbefore it destroys everyone.": 1,
	"Good Luck." : 1,
	"The fate of the entire world depends on you.": 1.5,
}

var current_index = 0

onready var subtitle_timer = $subtitle_timer
onready var letter_timer = $letter_timer
onready var text_label = $Background/Label


func show_subtitle(index):
	var text = INTRO_TEXT.keys()[index]
	var time = INTRO_TEXT[INTRO_TEXT.keys()[index]]
	text_label.text = ""
	for i in text:
		letter_timer.start()
		yield(letter_timer, "timeout")
		text_label.text += i
	subtitle_timer.wait_time = time
	subtitle_timer.start()


func _on_subtitle_timer_timeout():
	current_index += 1
	if INTRO_TEXT.keys().size() > current_index:
		show_subtitle(current_index)
	else:
		hide()


func _on_start_delay_timeout():
	show()
	show_subtitle(current_index)
