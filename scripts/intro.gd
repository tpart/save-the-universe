extends Spatial


const CLOUD_SPEED = 0.2

onready var intro_animation = $Camera/AnimationPlayer
onready var cloud_root = $clouds


func _ready():
	intro_animation.play("intro")
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "intro":
		intro_animation.play("intro_2")
	elif anim_name == "intro_2":
		intro_animation.play("intro_3")
	elif anim_name == "intro_3":
		Global.try(get_tree().change_scene("res://scenes/loading.tscn"))


func _input(_event):
	if Input.is_action_just_pressed("ui_cancel"):
		Global.try(get_tree().change_scene("res://scenes/loading.tscn"))


func _process(delta):
	cloud_root.translation.z += delta * CLOUD_SPEED
