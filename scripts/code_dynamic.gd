extends Control


const BACKGROUND_COLOR = Color("75c8ff")
const BACKGROUND_COLOR_RED = Color("ff0000")

var red := false
var new_nums = ""

onready var red_timer = $RedTimer
onready var color_rect = $ColorRect
onready var wrong_sound = $wrong
onready var code = $Code


func _ready():
	code.text = ""
	Global.try(Global.connect("wrong", self, "_on_wrong_code"))


func _on_wrong_code():
	color_rect.color = BACKGROUND_COLOR_RED
	new_nums = ""
	wrong_sound.play()
	red = true
	red_timer.start()


func _on_RedTimer_timeout():
	red = false
	code.text = new_nums
	color_rect.color = BACKGROUND_COLOR


func _on_digit_value_pressed(value):
	if not red:
		code.text += str(value) + " "
	else:
		new_nums += str(value) + " "
