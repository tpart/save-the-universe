# The Top Secret Mission To Save The Universe

This is the source code of [The Top Secret Mission To Save The Universe](https://tpart.itch.io/save-the-universe) 
developed by @tpart and @nokoe. It was created for the [GMTK Game Jam 2022](https://itch.io/jam/gmtk-jam-2022) using the [Godot](https://godotengine.org/) game engine.

![Image](assets/textures/splash.png)


# License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
