class_name Door

extends TileNode

signal door_closed


func open_door():
	if Global.weirdness >= Global.WeirdLevel.Aglkasmnil:
		randomize()
		var roll_of_the_dice: int = randi() % 5
		if roll_of_the_dice == 4:
			$Anim.play("open_weird")
			$open_weird.pitch_scale += rand_range(0.7, 1.3)
			$open_weird.play()
		else:
			$Anim.play("open")
			$open.pitch_scale += rand_range(0.7, 1.3)
			$open.play()
	else:
		$Anim.play("open")
		$open.pitch_scale += rand_range(0.7, 1.3)
		$open.play()


func close_door():
	$Anim.play("close")
	$close.pitch_scale += rand_range(0.7, 1.3)
	$close.play()


func _on_Anim_animation_finished(anim_name):
	if anim_name == "close":
		emit_signal("door_closed")


func _on_Area_body_entered(body):
	if body is KinematicBody:
		close_door()
